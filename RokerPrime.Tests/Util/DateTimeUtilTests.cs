﻿using RokerPrime.Util;
using System;
using System.Collections.Generic;
using Xunit;

namespace RokerPrime.Tests.Util
{
    public class DateTimeUtilTests
    {
        public class SeperateByWeekTests
        {
            private readonly List<DateTime> _dates = new List<DateTime>();
            private readonly DateTime _friday, _saturday, _sunday, _monday, _tuesday;

            public SeperateByWeekTests()
            {
                _friday = new DateTime(637269662020000000).Date;
                _saturday = _friday.AddDays(1);
                _sunday = _saturday.AddDays(1);
                _monday = _sunday.AddDays(1);
                _tuesday = _monday.AddDays(1);

                _dates.Add(_friday);
                _dates.Add(_saturday);
                _dates.Add(_sunday);
                _dates.Add(_monday);
                _dates.Add(_tuesday);
            }

            [Fact]
            public void BasicTest()
            {

                List<List<DateTime>> datesByWeek = DateTimeUtil.SeperateByWeek(_dates);

                Assert.Contains(_friday, datesByWeek[0]);
                Assert.Contains(_saturday, datesByWeek[0]);

                Assert.Contains(_sunday, datesByWeek[1]);
                Assert.Contains(_monday, datesByWeek[1]);
                Assert.Contains(_tuesday, datesByWeek[1]);

            }

            [Fact]
            public void CustomStartOfWeekTest()
            {
                List<List<DateTime>> datesByWeek = DateTimeUtil.SeperateByWeek(_dates, DayOfWeek.Saturday);

                Assert.Contains(_friday, datesByWeek[0]);

                Assert.Contains(_saturday, datesByWeek[1]);
                Assert.Contains(_sunday, datesByWeek[1]);
                Assert.Contains(_monday, datesByWeek[1]);
                Assert.Contains(_tuesday, datesByWeek[1]);
            }
        }
    }
}
