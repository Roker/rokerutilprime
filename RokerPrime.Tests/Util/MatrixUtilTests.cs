﻿using RokerPrime.Util;
using System;
using System.Linq;

namespace RokerPrime.Tests.Util
{
    public class MatrixUtilTests
    {
        public class MatrixTestTemplates
        {

            internal static void Test<T>(string testName, T[][] testMatrix, T[][] resultMatrix, T[][] expectedResultMatrix)
            {
                bool testSucceeded = resultMatrix.MatrixEquals(expectedResultMatrix);

                string statusString = testSucceeded ? "SUCCESS" : "FAILED";
                Console.WriteLine("{0,20} Result: {1}", testName, statusString);

                if (!testSucceeded)
                {
                    Console.WriteLine("Expected Matrix:");
                    Console.WriteLine(expectedResultMatrix.ToMatrixString());

                    Console.WriteLine("Result Matrix:");
                    Console.WriteLine(resultMatrix.ToMatrixString());
                }
            }

            internal static void Test<T>(string testName, T[][] testMatrix, T[] resultArray, T[] expectedResultArray)
            {
                bool testSucceeded = resultArray.SequenceEqual(expectedResultArray);

                string statusString = testSucceeded ? "SUCCESS" : "FAILED";
                Console.WriteLine("{0,20} Result: {1}", testName, statusString);

                if (!testSucceeded)
                {
                    Console.WriteLine("Expected Array:");
                    Console.WriteLine(expectedResultArray.ToArrayString());

                    Console.WriteLine("Result Array:");
                    Console.WriteLine(resultArray.ToArrayString());
                }
            }
        }

        public class MatrixRotateTests : MatrixUtilTests
        {
            public static void ClockwiseTest()
            {
                int[][] testMatrix = new int[][]{
                Enumerable.Range(0, 10).ToArray(),
                Enumerable.Range(10, 10).ToArray(),
                Enumerable.Range(20, 10).ToArray()
            };
                int[][] expectedResultMatrix = new int[][]{
                new int[] { 20, 10, 0 },
                new int[] { 21, 11, 1 },
                new int[] { 22, 12, 2 },
                new int[] { 23, 13, 3 },
                new int[] { 24, 14, 4 },
                new int[] { 25, 15, 5 },
                new int[] { 26, 16, 6 },
                new int[] { 27, 17, 7 },
                new int[] { 28, 18, 8 },
                new int[] { 29, 19, 9 }
            };

                int[][] resultMatrix = testMatrix.MatrixRotate(1);


                MatrixTestTemplates.Test("ClockwiseTest", testMatrix, resultMatrix, expectedResultMatrix);
            }

            public static void AntiClockwiseTest()
            {
                int[][] testMatrix = new int[][]{
                Enumerable.Range(0, 10).ToArray(),
                Enumerable.Range(10, 10).ToArray(),
                Enumerable.Range(20, 10).ToArray()
            };
                int[][] expectedResultMatrix = new int[][]{
                new int[] { 9, 19, 29 },
                new int[] { 8, 18, 28 },
                new int[] { 7, 17, 27 },
                new int[] { 6, 16, 26 },
                new int[] { 5, 15, 25 },
                new int[] { 4, 14, 24 },
                new int[] { 3, 13, 23 },
                new int[] { 2, 12, 22 },
                new int[] { 1, 11, 21 },
                new int[] { 0, 10, 20 }
            };

                int[][] resultMatrix = testMatrix.MatrixRotate(-1);


                MatrixTestTemplates.Test("AntiClockwiseTest", testMatrix, resultMatrix, expectedResultMatrix);
            }

            public static void FlipTest()
            {
                int[][] testMatrix = new int[][]{
                Enumerable.Range(0, 10).ToArray(),
                Enumerable.Range(10, 10).ToArray(),
                Enumerable.Range(20, 10).ToArray()
            };
                int[][] expectedResultMatrix = new int[][]{
                Enumerable.Range(20, 10).Reverse().ToArray(),
                Enumerable.Range(10, 10).Reverse().ToArray(),
                Enumerable.Range(0, 10).Reverse().ToArray()
            };

                int[][] resultMatrix = testMatrix.MatrixRotate(2);

                Console.WriteLine(testMatrix.ToMatrixString());
                Console.WriteLine(resultMatrix.ToMatrixString());


                MatrixTestTemplates.Test("AntiClockwiseTest", testMatrix, resultMatrix, expectedResultMatrix);
            }
        }

        public class SelectColumnTests : MatrixUtilTests
        {
            public static void BasicTest()
            {
                int[][] testMatrix = new int[][]{
                Enumerable.Range(0, 10).ToArray(),
                Enumerable.Range(10, 10).ToArray(),
                Enumerable.Range(20, 10).ToArray()
            };
                int[] expectedResultMatrix = new int[] { 3, 13, 23 };

                int[] resultMatrix = testMatrix.SelectColumn(3);


                MatrixTestTemplates.Test("Basic Test", testMatrix, resultMatrix, expectedResultMatrix);
            }
        }

        public class SubMatrixTests : MatrixUtilTests
        {
            public static void HalfTest()
            {
                int[][] testMatrix = new int[][]{
                Enumerable.Range(0, 10).ToArray(),
                Enumerable.Range(10, 10).ToArray(),
                Enumerable.Range(20, 10).ToArray()
            };
                int[][] expectedResultMatrix = new int[][]{
                Enumerable.Range(0, 10).ToArray(),
                Enumerable.Range(10, 10).ToArray()
            };

                int[][] resultMatrix = testMatrix.SubMatrix(0, 2);


                MatrixTestTemplates.Test("HalfTest", testMatrix, resultMatrix, expectedResultMatrix);
            }


            public static void BasicTest()
            {
                int[][] testMatrix = new int[][]{
                Enumerable.Range(0, 10).ToArray(),
                Enumerable.Range(10, 10).ToArray(),
                Enumerable.Range(20, 10).ToArray()
            };
                int[][] expectedResultMatrix = new int[][]{
                Enumerable.Range(5, 5).ToArray(),
                Enumerable.Range(15, 5).ToArray()
            };

                int[][] resultMatrix = testMatrix.SubMatrix(0, 2, 5, 5);


                MatrixTestTemplates.Test("BasicTest", testMatrix, resultMatrix, expectedResultMatrix);
            }

            public static void ZeroRowCountTest()
            {
                int[][] testMatrix = new int[][]{
                Enumerable.Range(0, 10).ToArray(),
                Enumerable.Range(10, 10).ToArray(),
                Enumerable.Range(20, 10).ToArray()
            };
                int[][] expectedResultMatrix = new int[][]{
                Enumerable.Range(10, 10).ToArray(),
                Enumerable.Range(20, 10).ToArray()
            };

                int[][] resultMatrix = testMatrix.SubMatrix(1, 0);


                MatrixTestTemplates.Test("ZeroRowCount Test", testMatrix, resultMatrix, expectedResultMatrix);
            }

            public static void ZeroColumnCountTest()
            {
                int[][] testMatrix = new int[][]{
                Enumerable.Range(0, 10).ToArray(),
                Enumerable.Range(10, 10).ToArray(),
                Enumerable.Range(20, 10).ToArray()
            };
                int[][] expectedResultMatrix = new int[][]{
                Enumerable.Range(3, 7).ToArray(),
                Enumerable.Range(13, 7).ToArray(),
                Enumerable.Range(23, 7).ToArray()
            };

                int[][] resultMatrix = testMatrix.SubMatrix(0, 0, 3, 0);


                MatrixTestTemplates.Test("ZeroColumnCountTest Test", testMatrix, resultMatrix, expectedResultMatrix);
            }

            public static void ZeroRowAndColumnCountTest()
            {
                int[][] testMatrix = new int[][]{
                Enumerable.Range(0, 10).ToArray(),
                Enumerable.Range(10, 10).ToArray(),
                Enumerable.Range(20, 10).ToArray()
            };
                int[][] expectedResultMatrix = new int[][]{
                Enumerable.Range(13, 7).ToArray(),
                Enumerable.Range(23, 7).ToArray()
            };

                int[][] resultMatrix = testMatrix.SubMatrix(1, 0, 3, 0);


                MatrixTestTemplates.Test("ZeroColumnCountTest Test", testMatrix, resultMatrix, expectedResultMatrix);
            }


        }

    }
}
