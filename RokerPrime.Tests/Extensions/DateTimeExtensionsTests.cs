﻿using RokerPrime.Extensions;
using System;
using Xunit;

namespace RokerPrime.Tests.Extensions
{
    public class DateTimeExtensionsTests
    {
        public class MostRecentDayOfWeekTests
        {
            [Fact]
            public void BasicTest()
            {
                DateTime startDate = new DateTime(637309633650000000L);
                DateTime expectedDate = new DateTime(637304449140000000L);

                DateTime actualDate = startDate.MostRecentDayOfWeek(DayOfWeek.Wednesday);

                Assert.Equal(expectedDate.Date, actualDate.Date);
            }

            [Fact]
            public void SameDayTest()
            {
                DateTime startDate = new DateTime(637309633650000000L);
                DateTime expectedDate = new DateTime(637309633650000000L);

                DateTime actualDate = startDate.MostRecentDayOfWeek(DayOfWeek.Tuesday);

                Assert.Equal(expectedDate.Date, actualDate.Date);
            }
        }

    }
}
