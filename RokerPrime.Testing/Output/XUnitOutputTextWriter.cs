﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Xunit.Abstractions;

namespace RokerPrime.Testing.Output
{
    public class XUnitOutputTextWriter : TextWriter
    {
        public override Encoding Encoding =>  Encoding.Default;

        private ITestOutputHelper _output;
        public XUnitOutputTextWriter(ITestOutputHelper output)
        {
            _output = output;
        }

        public override void Write(string value)
        {
            _output.WriteLine(value);
        }

        public override void Write(string format, params object[] args)
        {
            _output.WriteLine(format, args);
        }
    }
}
