﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RokerPrime.SQLite.Batch
{
    public class SQLiteBatchInserter<T> where T : new()
    {

        private const int ROWS_PER_INSERT = 500;

        private readonly SQLiteAsyncConnection _connection;

        private readonly Queue<T> _queue = new Queue<T>();

        public SQLiteBatchInserter(SQLiteAsyncConnection connection)
        {
            _connection = connection;
        }

        public SQLiteBatchInserter(SQLiteAsyncConnection connection, IEnumerable<T> ts) : this(connection)
        {
            Add(ts);
        }

        private void Add(IEnumerable<T> ts)
        {
            foreach (T t in ts)
            {
                _queue.Enqueue(t);
            }
        }

        public async Task ExecuteAsync()
        {
            while (_queue.Count > 0)
            {
                HashSet<T> values = new HashSet<T>();
                int count = Math.Min(ROWS_PER_INSERT, _queue.Count);
                for (int i = 0; i < count; i++)
                {
                    T t = _queue.Dequeue();
                    if (t == null)
                    {
                        break;
                    }
                    values.Add(t);
                }
                await SQLiteBatchFunctions.BatchInsert(_connection, values).ConfigureAwait(false);
            }
        }
    }
}
