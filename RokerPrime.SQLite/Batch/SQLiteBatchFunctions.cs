﻿using RokerPrime.SQLite.Util;
using RokerPrime.Util;
using SQLite;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace RokerPrime.SQLite.Batch
{
    public static class SQLiteBatchFunctions
    {
        private const string NULL_VALUE_STRING = "NULL";
        private const string BLOBBED_SUFFIX = "Blobbed";

        public static async Task BatchInsert<T>(SQLiteAsyncConnection connection, IEnumerable<T> ts) where T : new()
        {
            //Console.WriteLine("SQLiteBatchInsert - " + typeof(T).Name + " : " + ts.Count());

            TableMapping tableMapping = await connection.GetMappingAsync<T>().ConfigureAwait(false);
            //Console.WriteLine("tablemapping: " + tableMapping);

            string sql = BuildInsertOrIgnoreString<T>() + BuildValuesString(tableMapping, ts);
            //Console.WriteLine(sql);
            Task<int> insertTask = connection.ExecuteAsync(sql);
            await insertTask.ConfigureAwait(false);
            if (insertTask.Exception != null)
            {
                Console.WriteLine(sql);
                Console.WriteLine(insertTask.Exception.Message);
            }
        }

        private static string PrepareValueString<T>(T t, PropertyInfo property)
        {
            if (PropertyUtil.PropertyHasAttribute<AutoIncrementAttribute>(property))
            {
                return NULL_VALUE_STRING;
            }

            string sql = ValueToString(property.GetValue(t));
            return SQLiteUtil.CommaSurroundAndSanitize(sql);
        }


        private static string PrepareBlobbedValueString<T>(T t, PropertyInfo property)
        {
            object blobbedValue = property.GetValue(t);
            string sql = string.Empty;

            if (blobbedValue == null)
            {
                return NULL_VALUE_STRING;
            }
            if (blobbedValue is IList list)
            {
                foreach (object o in list)
                {
                    sql += ValueToString(o) + ",";
                }
                sql = sql.Substring(0, sql.Length - 1);
            }

            return SQLiteUtil.CommaSurroundAndSanitize(sql);
        }

        //private static PropertyInfo FindBlobbedProperty<T>(PropertyInfo property)
        //{
        //    return typeof(T).GetProperty(property.Name.Substring(0, BLOBBED_SUFFIX.Length));
        //}

        private static bool IsBlobbedProperty<T>(PropertyInfo property)
        {
            return property.Name.EndsWith(BLOBBED_SUFFIX);
        }

        private static string ValueToString(object value)
        {
            string sql;
            if (value == null)
            {
                return NULL_VALUE_STRING;
            }

            if (value is DateTime dt)
            {
                sql = dt.Ticks.ToString();
                //sql = dt.ToString("yyyy-MM-dd HH:mm:ss");
            }
            else
            {
                sql = value.ToString();
            }
            return sql;
        }


        //string funcs

        private static string BuildInsertOrIgnoreString<T>()
        {
            return string.Format("INSERT OR REPLACE INTO {0} ", typeof(T).Name);
        }

        private static string BuildValuesString<T>(TableMapping tableMapping, IEnumerable<T> data) where T : new()
        {
            string sql = " VALUES ";
            foreach (T t in data)
            {
                string st = "(";
                for (int i = 0; i < tableMapping.Columns.Length; i++)
                {
                    string val;
                    PropertyInfo property = typeof(T).GetProperty(tableMapping.Columns[i].PropertyName);
                    if (IsBlobbedProperty<T>(property))
                    {
                        val = PrepareBlobbedValueString(t, property);
                    }
                    else
                    {
                        val = PrepareValueString(t, property);
                    }
                    st += val + ",";
                }
                st = st.Substring(0, st.Length - 1) + "),";
                sql += st;
            }
            return sql.Substring(0, sql.Length - 1);
        }
    }
}
