﻿using RokerPrime.Util;
using SQLite;
using System;
using System.Reflection;

namespace RokerPrime.SQLite.Util
{
    public class SQLiteUtil
    {
        public static PropertyInfo GetPrimaryKeyProperty(Type type)
        {
            PropertyInfo propertyInfo = PropertyUtil.GetPropertyWithAttribute<PrimaryKeyAttribute>(type);
            if (propertyInfo == null)
            {
                throw new InvalidOperationException("PrimaryKey Property not found.");
            }
            return propertyInfo;
        }

        public static string CommaSurroundAndSanitize(string sql)
        {
            return "'" + SanitizeValue(sql) + "'";
        }

        public static string SanitizeValue(string sql)
        {
            return sql.Replace("'", "''");
        }
    }
}
