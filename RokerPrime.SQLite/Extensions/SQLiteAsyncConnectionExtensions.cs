﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace RokerPrime.SQLite.Extensions
{
    public static class SQLiteAsyncConnectionExtensions
    {
        public static Task<int> InsertOrReplaceAllAsync<T>(this SQLiteAsyncConnection connection, IEnumerable<T> items)
        {
            return connection.InsertAllAsync(items, "OR REPLACE");
        }
      
    }
}
