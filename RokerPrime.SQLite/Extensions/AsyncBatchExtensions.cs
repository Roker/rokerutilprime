﻿using MoreLinq;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace RokerPrime.SQLite.Extensions
{
    public static class AsyncBatchExtensions
    {

        private const int DEFAULT_BATCH_SIZE = 500;


        public static async Task BatchDeleteAsync<T, Y>(this AsyncTableQuery<T> table,
            IEnumerable<Y> list, Func<IEnumerable<Y>, Expression<Func<T, bool>>> expressionFunc,
                int batchSize = DEFAULT_BATCH_SIZE) where T : new()
        {

            foreach (IEnumerable<Y> batch in list.Batch(batchSize))
            {
                try
                {
                    await table.DeleteAsync(expressionFunc(batch)).ConfigureAwait(false);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.StackTrace);
                }
            }
        }
    }
}
