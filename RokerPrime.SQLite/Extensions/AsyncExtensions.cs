﻿using SQLite;
using System.Threading.Tasks;

namespace RokerPrime.SQLite.Extensions
{
    public static class AsyncExtensions
    {
        public static Task VacuumAsync(this SQLiteAsyncConnection connection)
        {
            return connection.ExecuteAsync("vacuum");
        }
    }
}
