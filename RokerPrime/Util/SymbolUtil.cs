﻿namespace RokerPrime.Util
{
    public static class SymbolUtil
    {
        public static readonly string Checkmark = ((char)0x221A).ToString();

        public static string CheckmarkOrEmpty(bool val)
        {
            return val ? Checkmark : "";
        }
    }
}
