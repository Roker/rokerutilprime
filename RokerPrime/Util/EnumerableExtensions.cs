﻿using System;
using System.Collections.Generic;

namespace RokerPrime.Util
{
    public static class EnumerableExtensions
    {
        public static List<U> GetPropertyList<T, U>(this IEnumerable<T> list, Func<T, U> getProp)
        {
            List<U> values = new List<U>();
            foreach (T t in list)
            {
                values.Add(getProp(t));
            }
            return values;
        }
    }
}
