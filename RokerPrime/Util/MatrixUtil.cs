﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RokerPrime.Util
{
    public static class MatrixUtil
    {
        private const int DEFAULT_MATRIX_CELL_STRING_LENGTH = 3;

        public static T[] SelectColumn<T>(this T[][] matrix, int columnIndex)
        {
            T[] column = new T[matrix.Length];

            for (int i = 0; i < matrix.Length; i++)
            {
                column[i] = matrix[i][columnIndex];
            }
            return column;
        }

        public static T[][] SubMatrix<T>(this T[][] oldMatrix, int startRow, int rowCount, int startColumn = 0, int columnCount = 0)
        {
            if (rowCount == 0)
            {
                rowCount = oldMatrix.Length - startRow;
            }

            T[][] newMatrix = new T[rowCount][];

            for (int rowIndex = startRow; rowIndex < startRow + rowCount; rowIndex++)
            {
                IEnumerable<T> newRow = oldMatrix[rowIndex];
                if (startColumn > 0)
                {
                    newRow = newRow.Skip(startColumn);
                }

                if (columnCount > 0)
                {
                    newRow = newRow.Take(columnCount);
                }

                newMatrix[rowIndex - startRow] = newRow.ToArray();
            }

            return newMatrix;
        }


        public static T[][] MatrixRotate<T>(this T[][] oldMatrix, int inputDirection = 1)
        {
            int direction = DirectionFromInt(inputDirection);
            int oldColumnCount = oldMatrix[0].Length;
            int oldRowCount = oldMatrix.Length;

            if (direction == 0)
            {
                return oldMatrix;
            }
            else if (direction == 1)
            {
                T[][] newMatrix = new T[oldColumnCount][];
                for (int oldColumnIndex = 0; oldColumnIndex < oldColumnCount; oldColumnIndex++)
                {
                    newMatrix[oldColumnIndex] = new T[oldRowCount];
                    for (int oldRowIndex = 0; oldRowIndex < oldMatrix.Length; oldRowIndex++)
                    {
                        newMatrix[oldColumnIndex][oldRowIndex] = oldMatrix[oldRowCount - oldRowIndex - 1][oldColumnIndex];
                    }
                }
                return newMatrix;
            }
            else if (direction == -1)
            {
                T[][] newMatrix = new T[oldColumnCount][];
                for (int oldColumnIndex = 0; oldColumnIndex < oldColumnCount; oldColumnIndex++)
                {
                    newMatrix[oldColumnIndex] = new T[oldRowCount];
                    for (int oldRowIndex = 0; oldRowIndex < oldMatrix.Length; oldRowIndex++)
                    {
                        newMatrix[oldColumnIndex][oldRowIndex] = oldMatrix[oldRowIndex][oldColumnCount - oldColumnIndex - 1];
                    }
                }
                return newMatrix;
            }
            else if (direction == 2)
            {
                T[][] newMatrix = new T[oldRowCount][];
                for (int oldRowIndex = 0; oldRowIndex < oldMatrix.Length; oldRowIndex++)
                {
                    newMatrix[oldRowIndex] = new T[oldColumnCount];

                    for (int oldColumnIndex = 0; oldColumnIndex < oldColumnCount; oldColumnIndex++)
                    {
                        newMatrix[oldRowIndex][oldColumnIndex] = oldMatrix[oldRowCount - oldRowIndex - 1][oldColumnCount - oldColumnIndex - 1];

                    }
                }

                return newMatrix;
            }
            else
            {
                throw new Exception();
            }

        }

        private static int DirectionFromInt(int direction)
        {
            Console.WriteLine("in: " + direction);
            direction %= 4;
            if (direction == 3)
            {
                direction = -1;
            }
            else if (direction == -3)
            {
                direction = 1;
            }
            Console.WriteLine("out: " + direction);

            return direction;
        }

        public static string ToMatrixString<T>(this T[][] matrix, int spacing = DEFAULT_MATRIX_CELL_STRING_LENGTH)
        {
            string output = string.Empty;

            foreach (T[] row in matrix)
            {
                output += row.ToArrayString(spacing).Replace("\n", "") + "\n";
            }
            return output;
        }

        public static string ToArrayString<T>(this T[] row, int spacing = DEFAULT_MATRIX_CELL_STRING_LENGTH)
        {
            string output = "[";

            foreach (T t in row)
            {
                output += string.Format("{0," + spacing + "},", t.ToString());
            }

            output += "]";

            return output;
        }

        public static bool MatrixEquals<T>(this T[][] matrix, T[][] other)
        {
            if (matrix.Length != other.Length) return false;

            for (int i = 0; i < matrix.Length; i++)
            {
                if (!matrix[i].SequenceEqual(other[i]))
                {
                    return false;
                }
            }
            return true;
        }

        public static void MatrixPositionFromIndex(int index, int columnCount, out int row, out int column)
        {
            row = (int)Math.Floor((double)index / columnCount);
            column = index % columnCount;
        }

    }
}
