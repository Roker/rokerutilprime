﻿using System;
using System.IO;
using System.Net;

namespace RokerPrime.Util
{
    public static class WebUtil
    {
        public static string GetAndReadToEnd(HttpWebRequest request)
        {
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                string responseString = reader.ReadToEnd();
                Console.WriteLine(responseString);
                return responseString;
            }
        }
    }
}
