﻿using System;
using System.Text;

namespace RokerPrime.Util
{
    public static class EncodeUtil
    {
        public static string Base64Encode(string s)
        {
            var bytes = Encoding.UTF8.GetBytes(s);
            return Convert.ToBase64String(bytes);
        }
    }
}
