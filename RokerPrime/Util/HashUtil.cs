﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace RokerPrime.Util
{
    public class HashUtil
    {
        public static byte[] RNGBytes(int length)
        {
            byte[] data = new byte[length];

            using (var rng = new RNGCryptoServiceProvider())
            {
                rng.GetBytes(data);
            }
            return data;
        }

        public static string RNGBase64(int length)
        {
            byte[] data = RNGBytes(length);
            return Convert.ToBase64String(data);
        }

        public static string HashSha256(string text)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(text);
            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            string hashString = string.Empty;
            foreach (byte x in hash)
            {
                hashString += string.Format("{0:x2}", x);
            }
            return hashString;
        }
    }
}
