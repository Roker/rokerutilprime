﻿namespace RokerPrime.Util
{
    public static class HTMLUtil
    {
        public static string[][] RemoveTags(string[][] input)
        {
            for (int i = 0; i < input.Length; i++)
            {
                input[i] = RemoveTags(input[i]);
            }
            return input;
        }

        public static string[] RemoveTags(string[] input)
        {
            for (int i = 0; i < input.Length; i++)
            {
                input[i] = RemoveTags(input[i]);
            }
            return input;
        }

        public static string RemoveTags(string input)
        {
            while (input.Contains("<") && input.Contains(">"))
            {
                int start = input.IndexOf("<");
                int end = input.IndexOf(">") + 1;
                input = input.Substring(0, start) + "\n" + input.Substring(end, input.Length - end);
            }
            return input;
        }

    }
}
