﻿using RokerPrime.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RokerPrime.Util
{
    public static class DateTimeUtil
    {
        public static DateTime FromJavaTimestamp(long timestamp)
        {
            return FromUnixTimestamp(timestamp / 1000L);
        }

        public static long ToJavaTimeStamp(DateTime dateTime)
        {
            return ToUnixTimeStamp(dateTime) * 1000L;
        }
        public static DateTime FromUnixTimestamp(long timestamp)
        {
            DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dt = dt.AddSeconds(timestamp).ToLocalTime();
            return dt;
        }

        public static long ToUnixTimeStamp(DateTime dateTime)
        {
            return (long)dateTime.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
        }

        public static List<List<DateTime>> SeperateByWeek(List<DateTime> dateTimes,
            DayOfWeek firstDayOfWeek = DayOfWeek.Sunday)
        {
            return SeperateByWeek(dateTimes, t => t, firstDayOfWeek);
        }

        public static List<List<T>> SeperateByWeek<T>(List<T> ts, Func<T, DateTime> func,
            DayOfWeek firstDayOfWeek = DayOfWeek.Sunday)
        {
            Dictionary<DateTime, List<T>> tsByWeek = new Dictionary<DateTime, List<T>>();
            foreach (T t in ts)
            {
                DateTime firstDateInWeek = func(t).MostRecentDayOfWeek(firstDayOfWeek);

                if (!tsByWeek.ContainsKey(firstDateInWeek))
                {
                    tsByWeek[firstDateInWeek] = new List<T>();
                }

                tsByWeek[firstDateInWeek].Add(t);
            }
            return tsByWeek.Values.ToList();
        }
    }
}
