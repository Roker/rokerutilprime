﻿using Newtonsoft.Json;
using System.IO;

namespace RokerPrime.Util
{
    public static class JsonUtil
    {
        public static T DeserializeFromFile<T>(string fileName)
        {
            try
            {
                string json = File.ReadAllText(fileName);
                return JsonConvert.DeserializeObject<T>(json);
            }
            catch
            {
                return default;
            }
        }

        public static bool SerializeToFile(object o, string fileName)
        {
            try
            {
                string json = JsonConvert.SerializeObject(o);
                File.WriteAllText(fileName, json);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
