﻿using System.Collections.ObjectModel;
using System.Text;

namespace RokerPrime.Util
{
    public static class MatrixOperations
    {
        public static void MatrixAdd<T>(this Collection<T[]> matrix, T newObject, int columnCount, bool replaceRows = false) where T : class
        {
            if (matrix.Count > 0)
            {
                T[] row = matrix[matrix.Count - 1];
                for (int i = 0; i < columnCount; i++)
                {
                    if (row[i] == null)
                    {
                        int index = ((matrix.Count - 1) * columnCount) + i;
                        matrix.MatrixInsert(index, newObject, columnCount, replaceRows);
                        return;
                    }
                }
            }
            matrix.MatrixInsert(matrix.Count * columnCount, newObject, columnCount, replaceRows);
        }


        public static void MatrixReplace<T>(this Collection<T[]> matrix, int index,
            T t, int columnCount, bool replaceRows = false)
        {
            MatrixUtil.MatrixPositionFromIndex(index, columnCount, out int rowIndex, out int columnIndex);

            matrix[rowIndex] = RowReplace(matrix[rowIndex], columnIndex, t, replaceRows);
        }



        public static void MatrixMove<T>(this Collection<T[]> matrix,
                    int oldIndex, int newIndex, int columnCount, bool replaceRows = false) where T : class
        {
            MatrixUtil.MatrixPositionFromIndex(oldIndex, columnCount, out int oldRowIndex, out int oldColumnIndex);
            MatrixUtil.MatrixPositionFromIndex(newIndex, columnCount, out int newRowIndex, out int newColumnIndex);


            if (oldIndex < newIndex)
            {

                T t = matrix[oldRowIndex][oldColumnIndex];
                if (oldRowIndex == newRowIndex)
                {
                    matrix[newRowIndex] =
                        ReverseRowInsert(matrix[newRowIndex], t, out _, newColumnIndex, oldColumnIndex, replaceRows: replaceRows);
                    return;
                }
                matrix[newRowIndex] = ReverseRowInsert(matrix[newRowIndex], t, out T popped, newColumnIndex, replaceRows: replaceRows);

                for (int i = newRowIndex - 1; i > oldRowIndex; i--)
                {
                    matrix[i] = ReverseRowInsert(matrix[i], popped, out popped, replaceRows: replaceRows);
                }

                matrix[oldRowIndex] = ReverseRowInsert(matrix[oldRowIndex], popped, out _, -1, oldColumnIndex, replaceRows);
            }
            else if (oldIndex > newIndex)
            {
                T t = matrix[oldRowIndex][oldColumnIndex];
                if (oldRowIndex == newRowIndex)
                {
                    matrix[newRowIndex] =
                        RowInsert(matrix[newRowIndex], t, out _, newColumnIndex, oldColumnIndex, replaceRows: replaceRows);
                    return;
                }
                matrix[newRowIndex] = RowInsert(matrix[newRowIndex], t, out T popped, newColumnIndex, replaceRows: replaceRows);

                for (int i = newRowIndex + 1; i < oldRowIndex; i++)
                {
                    matrix[i] = RowInsert(matrix[i], popped, out popped, replaceRows: replaceRows);
                }

                matrix[oldRowIndex] = RowInsert(matrix[oldRowIndex], popped, out _, 0, oldColumnIndex, replaceRows: replaceRows);
            }
        }

        public static void MatrixRemove<T>(this Collection<T[]> matrix, int index,
            int columnCount, bool replaceRows = false) where T : class
        {
            MatrixUtil.MatrixPositionFromIndex(index, columnCount, out int rowIndex, out int columnIndex);

            T popped = null;
            for (int i = matrix.Count - 1; i > rowIndex; i--)
            {
                matrix[i] = ReverseRowInsert(matrix[i], popped, out popped, replaceRows: replaceRows);
            }

            matrix[rowIndex] = ReverseRowInsert(matrix[rowIndex], popped, out _, -1, columnIndex, replaceRows: replaceRows);

            if (matrix[matrix.Count - 1].RowEmpty())
            {
                matrix.RemoveAt(matrix.Count - 1);
            }
        }

        public static void MatrixInsert<T>(this Collection<T[]> matrix, int index, T newObject,
            int columnCount, bool replaceRows = false) where T : class
        {
            MatrixUtil.MatrixPositionFromIndex(index, columnCount, out int rowIndex, out int columnIndex);

            T popped;
            if (matrix.Count > rowIndex)
            {
                matrix[rowIndex] = RowInsert(matrix[rowIndex], newObject, out popped, columnIndex, replaceRows: replaceRows);
            }
            else
            {
                T[] row = new T[columnCount];
                row[0] = newObject;
                matrix.Add(row);
                return;
            }

            for (int i = rowIndex + 1; i < matrix.Count; i++)
            {
                matrix[i] = RowInsert(matrix[i], popped, out popped, replaceRows: replaceRows);
            }

            if (popped != null)
            {
                T[] newRow = new T[columnCount];
                newRow[0] = popped;
                matrix.Add(newRow);
            }
        }


        public static string MatrixPrint<T>(this Collection<T[]> matrix)
        {
            StringBuilder s = new StringBuilder();
            foreach (T[] row in matrix)
            {
                s.Append(row.RowPrint() + "\n");
            }
            return s.ToString();
        }

        public static string RowPrint<T>(this T[] row)
        {
            return string.Join(",", row);
        }

        //public static T RowRemove<T>(this T[] row, int columnIndex, bool replaceRows = false) where T : class
        //{
        //    return row.ReverseRowInsert(null, -1, columnIndex);
        //}



        private static T[] RowCopy<T>(T[] row, bool replaceRows)
        {
            if (!replaceRows) return row;
            T[] newRow = new T[row.Length];
            row.CopyTo(newRow, 0);
            return newRow;
        }
        private static bool RowEmpty<T>(this T[] row) where T : class
        {
            foreach (T t in row)
            {
                if (t != null)
                {
                    return false;
                }
            }
            return true;
        }

        private static T[] RowInsert<T>(T[] row, T newObject, out T popped, int insertColumnIndex = 0,
            int poppedColumnIndex = -1, bool replaceRows = false)
        {
            if (poppedColumnIndex == -1)
            {
                poppedColumnIndex = row.Length - 1;
            }
            popped = row[poppedColumnIndex];
            for (int i = poppedColumnIndex - 1; i >= insertColumnIndex; i--)
            {
                row[i + 1] = row[i];
            }
            row[insertColumnIndex] = newObject;

            return RowCopy(row, replaceRows);
        }

        private static T[] ReverseRowInsert<T>(T[] row, T newObject, out T popped,
            int insertColumnIndex = -1, int poppedColumnIndex = 0, bool replaceRows = false)
        {
            if (insertColumnIndex == -1)
            {
                insertColumnIndex = row.Length - 1;
            }
            popped = row[poppedColumnIndex];
            for (int i = poppedColumnIndex; i < insertColumnIndex; i++)
            {
                row[i] = row[i + 1];
            }
            row[insertColumnIndex] = newObject;
            return RowCopy(row, replaceRows);
        }

        private static T[] RowReplace<T>(T[] row, int columnIndex, T t, bool replaceRows)
        {
            row[columnIndex] = t;
            return RowCopy(row, replaceRows);
        }
    }
}
