﻿using System;
using System.Reflection;

namespace RokerPrime.Util
{
    public static class PropertyUtil
    {
        public static PropertyInfo GetPropertyWithAttribute<U>(Type t)
        {
            PropertyInfo[] properties = t.GetProperties();
            foreach (PropertyInfo property in properties)
            {
                object[] attributes = property.GetCustomAttributes(true);
                foreach (object attribute in attributes)
                {
                    if (attribute is U)
                    {
                        return property;
                    }
                }
            }
            return null;
        }

        public static bool PropertyHasAttribute<T>(PropertyInfo property)
        {
            object[] attributes = property.GetCustomAttributes(true);
            foreach (object attribute in attributes)
            {
                if (attribute is T)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
