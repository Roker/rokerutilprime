﻿namespace RokerPrime.Util
{
    public static class ClassUtil
    {
        public static string ClassNameFromGenericType(object o)
        {
            string fullName = o.GetType().ToString();
            fullName = fullName.Substring(fullName.IndexOf("["));
            fullName = fullName.Substring(1, fullName.Length - 2);
            string[] splitName = fullName.Split('.');
            return splitName[splitName.Length - 1];
        }
    }
}
