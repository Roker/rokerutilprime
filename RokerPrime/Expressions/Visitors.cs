﻿using System;
using System.Linq.Expressions;

namespace RokerPrime.Expressions
{
    //https://docs.microsoft.com/en-us/dotnet/csharp/expression-trees-interpreting
    // Base Visitor class: 
    public abstract class Visitor
    {
        private readonly Expression _node;

        protected Visitor(Expression node)
        {
            _node = node;
        }

        public abstract void Visit(string prefix);

        public ExpressionType NodeType => this._node.NodeType;
        public static Visitor CreateFromExpression(Expression node)
        {
            switch (node.NodeType)
            {
                case ExpressionType.Constant:
                    return new ConstantVisitor((ConstantExpression)node);
                case ExpressionType.Lambda:
                    return new LambdaVisitor((LambdaExpression)node);
                case ExpressionType.Parameter:
                    return new ParameterVisitor((ParameterExpression)node);
                case ExpressionType.Add:
                case ExpressionType.Equal:
                case ExpressionType.NotEqual:
                case ExpressionType.Multiply:
                    return new BinaryVisitor((BinaryExpression)node);
                case ExpressionType.Conditional:
                    return new ConditionalVisitor((ConditionalExpression)node);
                case ExpressionType.Call:
                    return new MethodCallVisitor((MethodCallExpression)node);
                default:
                    Console.Error.WriteLine($"Node not processed yet: {node.NodeType}");
                    return default;
            }
        }
    }

    // Lambda Visitor
    public class LambdaVisitor : Visitor
    {
        private readonly LambdaExpression _node;
        public LambdaVisitor(LambdaExpression node) : base(node)
        {
            this._node = node;
        }

        public override void Visit(string prefix)
        {
            Console.WriteLine($"{prefix}This expression is a {NodeType} expression type");
            Console.WriteLine($"{prefix}The name of the lambda is {(_node.Name ?? "<null>")}");
            Console.WriteLine($"{prefix}The return type is {_node.ReturnType}");
            Console.WriteLine($"{prefix}The expression has {_node.Parameters.Count} argument(s). They are:");
            // Visit each parameter:
            foreach (var argumentExpression in _node.Parameters)
            {
                var argumentVisitor = Visitor.CreateFromExpression(argumentExpression);
                argumentVisitor.Visit(prefix + "\t");
            }
            Console.WriteLine($"{prefix}The expression body is:");
            // Visit the body:
            var bodyVisitor = Visitor.CreateFromExpression(_node.Body);
            bodyVisitor.Visit(prefix + "\t");
        }
    }

    // Binary Expression Visitor:
    public class BinaryVisitor : Visitor
    {
        private readonly BinaryExpression _node;
        public BinaryVisitor(BinaryExpression node) : base(node)
        {
            this._node = node;
        }

        public override void Visit(string prefix)
        {
            Console.WriteLine($"{prefix}This binary expression is a {NodeType} expression");
            var left = Visitor.CreateFromExpression(_node.Left);
            Console.WriteLine($"{prefix}The Left argument is:");
            if (left != null)
            {
                left.Visit(prefix + "\t");
            }
            var right = Visitor.CreateFromExpression(_node.Right);
            Console.WriteLine($"{prefix}The Right argument is:");
            if (right != null)
            {
                right.Visit(prefix + "\t");
            }
        }
    }

    // Parameter visitor:
    public class ParameterVisitor : Visitor
    {
        private readonly ParameterExpression _node;
        public ParameterVisitor(ParameterExpression node) : base(node)
        {
            _node = node;
        }

        public override void Visit(string prefix)
        {
            Console.WriteLine($"{prefix}This is an {NodeType} expression type");
            Console.WriteLine($"{prefix}Type: {_node.Type}, Name: {_node.Name}, ByRef: {_node.IsByRef}");
        }
    }

    // Constant visitor:
    public class ConstantVisitor : Visitor
    {
        private readonly ConstantExpression _node;
        public ConstantVisitor(ConstantExpression node) : base(node)
        {
            this._node = node;
        }

        public override void Visit(string prefix)
        {
            Console.WriteLine($"{prefix}This is an {NodeType} expression type");
            Console.WriteLine($"{prefix}The type of the constant value is {_node.Type}");
            Console.WriteLine($"{prefix}The value of the constant value is {_node.Value}");
        }
    }

    public class ConditionalVisitor : Visitor
    {
        private readonly ConditionalExpression _node;
        public ConditionalVisitor(ConditionalExpression node) : base(node)
        {
            this._node = node;
        }

        public override void Visit(string prefix)
        {
            Console.WriteLine($"{prefix}This expression is a {NodeType} expression");
            var testVisitor = Visitor.CreateFromExpression(_node.Test);
            Console.WriteLine($"{prefix}The Test for this expression is:");
            testVisitor.Visit(prefix + "\t");
            var trueVisitor = Visitor.CreateFromExpression(_node.IfTrue);
            Console.WriteLine($"{prefix}The True clause for this expression is:");
            trueVisitor.Visit(prefix + "\t");
            var falseVisitor = Visitor.CreateFromExpression(_node.IfFalse);
            Console.WriteLine($"{prefix}The False clause for this expression is:");
            falseVisitor.Visit(prefix + "\t");
        }
    }

    public class MethodCallVisitor : Visitor
    {
        private readonly MethodCallExpression _node;
        public MethodCallVisitor(MethodCallExpression node) : base(node)
        {
            this._node = node;
        }

        public override void Visit(string prefix)
        {
            Console.WriteLine($"{prefix}This expression is a {NodeType} expression");
            if (_node.Object == null)
                Console.WriteLine($"{prefix}This is a static method call");
            else
            {
                Console.WriteLine($"{prefix}The receiver (this) is:");
                var receiverVisitor = Visitor.CreateFromExpression(_node.Object);
                receiverVisitor.Visit(prefix + "\t");
            }

            var methodInfo = _node.Method;
            Console.WriteLine($"{prefix}The method name is {methodInfo.DeclaringType}.{methodInfo.Name}");
            // There is more here, like generic arguments, and so on.
            Console.WriteLine($"{prefix}The Arguments are:");
            foreach (var arg in _node.Arguments)
            {
                var argVisitor = Visitor.CreateFromExpression(arg);
                argVisitor.Visit(prefix + "\t");
            }
        }
    }
}
