﻿
namespace RokerPrime.Args.EventArgs
{
    public class StringArgs : System.EventArgs
    {
        private string _arg;
        public void SetArg(string arg)
        {
            _arg = arg;
        }

        public string GetArg()
        {
            return _arg;
        }
    }
}
