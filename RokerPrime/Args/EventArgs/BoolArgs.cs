﻿
namespace RokerPrime.Args.EventArgs
{
    public class BoolArgs : System.EventArgs
    {
        public static readonly BoolArgs True = new BoolArgs(true);
        public static readonly BoolArgs False = new BoolArgs(false);

        private bool _arg;

        public BoolArgs(bool arg)
        {
            _arg = arg;
        }

        public void SetArg(bool arg)
        {
            _arg = arg;
        }

        public bool GetArg()
        {
            return _arg;
        }
    }
}
