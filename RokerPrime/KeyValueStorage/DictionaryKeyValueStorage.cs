﻿using PushClientLib.KeyValueStorage;
using System.Collections.Generic;

namespace PushTest
{
    public class DictionaryKeyValueStorage : IKeyValueStorage
    {
        private readonly Dictionary<string, object> _dictionary = new Dictionary<string, object>();
        public T Get<T>(string key) where T : new()
        {
            return (T)_dictionary[key];
        }

        public T Get<T>(string key, T defaultValue)
        {
            try
            {
                return (T)_dictionary[key];
            }
            catch
            {
                return default;
            }
        }

        public void Set<T>(string key, T value)
        {
            _dictionary[key] = value;
        }
    }
}
