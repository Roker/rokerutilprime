﻿namespace PushClientLib.KeyValueStorage
{
    public interface IKeyValueStorage
    {
        void Set<T>(string key, T value);
        T Get<T>(string key) where T : new();
        T Get<T>(string key, T defaultValue);
    }
}
