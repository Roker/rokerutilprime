﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace RokerPrime.Tasks
{
    //https://stackoverflow.com/users/1159478/servy
    //https://stackoverflow.com/questions/25691679/best-way-in-net-to-manage-queue-of-tasks-on-a-separate-single-thread
    public class TaskQueue
    {
        private readonly SemaphoreSlim _semaphore;

        private bool _isPaused = false;

        public void Resume()
        {
            _isPaused = false;
        }
        public void Pause()
        {
            _isPaused = true;
        }

        public TaskQueue(int threads = 1)
        {
            _semaphore = new SemaphoreSlim(threads);
        }

        public async Task<T> Enqueue<T>(Func<Task<T>> taskGenerator)
        {
            await AwaitNotPaused().ConfigureAwait(false);
            await _semaphore.WaitAsync().ConfigureAwait(false);
            try
            {
                return await taskGenerator().ConfigureAwait(false);
            }
            finally
            {
                _semaphore.Release();
            }
        }

        public async Task Enqueue(Func<Task> taskGenerator)
        {
            await AwaitNotPaused().ConfigureAwait(false);
            await _semaphore.WaitAsync().ConfigureAwait(false);
            try
            {
                await taskGenerator().ConfigureAwait(false);
            }
            finally
            {
                _semaphore.Release();
            }
        }

        private async Task AwaitNotPaused()
        {
            while (_isPaused)
            {
                await Task.Delay(200).ConfigureAwait(false);
            }
        }
    }
}
