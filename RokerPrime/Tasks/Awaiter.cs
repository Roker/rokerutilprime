﻿using System.Threading;
using System.Threading.Tasks;

namespace RokerPrime.Tasks
{
    public class Awaiter
    {
        private CancellationTokenSource _waitCancellationTokenSource;

        public async Task Wait()
        {
            _waitCancellationTokenSource = new CancellationTokenSource();
            while (!_waitCancellationTokenSource.IsCancellationRequested)
            {
                try
                {
                    await Task.Delay(10000, _waitCancellationTokenSource.Token).ConfigureAwait(false);
                }
                catch
                {
                }
            }

        }

        public void Release()
        {
            _waitCancellationTokenSource?.Cancel();
        }
    }
}
