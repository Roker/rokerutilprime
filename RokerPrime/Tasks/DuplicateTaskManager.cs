﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace RokerPrime.Tasks
{
    public class DuplicateTaskManager
    {
        private const int DELAY = 1000;
        private readonly CancellationTokenSource _tokenSource;

        private Func<Task> _func;

        private Task _task;

        private readonly int _delay;

        public DuplicateTaskManager()
        {
            _delay = DELAY;
        }

        public DuplicateTaskManager(int delay)
        {
            _delay = delay;
        }

        public void StartExecute(Func<Task> func)
        {
            _func = func;

            if (_task == null || (_task.Status != TaskStatus.Running && _task.Status != TaskStatus.WaitingForActivation))
            {
                _task = Task.Run(Execute);
            }
        }

        private async Task Execute()
        {
            await Task.Delay(_delay).ConfigureAwait(false);
            //Console.WriteLine("token: " + cancellationToken);
            //cancellationToken.ThrowIfCancellationRequested();
            await _func.Invoke().ConfigureAwait(false);
        }
    }
}
