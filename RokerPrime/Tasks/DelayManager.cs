﻿using System.Threading.Tasks;

namespace RokerPrime.Tasks
{
    public class DelayManager
    {
        private readonly int _delay;
        public bool IsReady { get; private set; } = true;

        public DelayManager(int delay)
        {
            _delay = delay;
        }

        public async void StartDelay()
        {
            IsReady = false;
            await Task.Delay(_delay);
            IsReady = true;
        }
    }
}
