﻿using System;
using System.Threading.Tasks;

//TODO: document/test - tasks should probably be a seperate project
namespace RokerPrime.Tasks
{
    public class AwaitableDuplicateTask
    {
        private readonly object _lock = new object();
        public Func<Task> Func {
            set {
                lock (_lock)
                {
                    _func = value;
                }
                RunTask();
            }
        }

        private Func<Task> _func;

        private Task _task;

        public AwaitableDuplicateTask()
        {
        }
        public AwaitableDuplicateTask(Func<Task> func)
        {
            Func = func;
        }


        private void RunTask()
        {
            if ((_task == null || _task.Status != TaskStatus.Running) && _func != null)
            {
                lock (_lock)
                {
                    Console.WriteLine("Starting task");
                    _task = Task.Run(_func);
                    _func = null;

                }
            }
            else
            {
                Console.WriteLine("task in prog");
                Console.WriteLine(_task == null);
                Console.WriteLine(_task.Status != TaskStatus.Running);
                Console.WriteLine(_func != null);


            }
        }

        public async Task AwaitCompletion()
        {
            await _task;
            if (_func != null)
            {
                RunTask();
                await AwaitCompletion();
            }
        }
    }
}
