﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace RokerPrime.Reflection
{
    public static class TypeFunctions
    {
        //https://stackoverflow.com/questions/8645430/get-all-types-implementing-specific-open-generic-type
        public static IEnumerable<Type> GetAllTypesImplementingOpenGenericType(Type openGenericType, Assembly assembly)
        {
            return from x in assembly.GetTypes()
                   from z in x.GetInterfaces()
                   let baseType = x.BaseType
                   where
                   (baseType != null && baseType.IsGenericType &&
                   openGenericType.IsAssignableFrom(baseType.GetGenericTypeDefinition())) ||
                   (z.IsGenericType &&
                   openGenericType.IsAssignableFrom(z.GetGenericTypeDefinition()))
                   select x;
        }
    }
}
