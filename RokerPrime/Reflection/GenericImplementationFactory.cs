﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Reflection;
//using System.Text;

//namespace RokerPrime.Reflection
//{
//    public class GenericImplementationFactory<T>
//    {
//        private readonly Dictionary<Type, Type> implementationDictionary;
//        public GenericImplementationFactory(Assembly assembly, Func<Type, bool> constraint)
//        {
//            IEnumerable<Type> modbuilderImpls =
//                TypeFunctions.GetAllTypesImplementingOpenGenericType(
//                        typeof(T),
//                        assembly
//                    ).Where(x => !x.IsAbstract);

//            implementationDictionary = new Dictionary<Type, Type>();
//            foreach (Type modbuilderImpl in modbuilderImpls)
//            {
//                try
//                {
//                    Type genericType = modbuilderImpl.GetInterfaces()
//                    .Where(x => x.GetGenericTypeDefinition() == typeof(T))
//                    .First().GenericTypeArguments[0];
//                    implementationDictionary.Add(genericType, modbuilderImpl);
//                }
//                catch (Exception e)
//                {
//                    Console.WriteLine(
//                        "Error getting modbuilder type - "
//                        + modbuilderImpl.ToString() + " :" + e.StackTrace);
//                }
//            }
//        }

//        internal T<U> GetModBuilder<U>()
//        {
//            Type modBuilderType;
//            if (modBuilderDictionary.TryGetValue(typeof(T), out modBuilderType))
//            {
//                return (IQBModBuilder<T>)Activator.CreateInstance(modBuilderType);
//            }
//            else
//            {
//                throw new NotSupportedException("ModBuilder for type " + typeof(T).ToString() + " not found");
//            }
//        }
//    }
//}
