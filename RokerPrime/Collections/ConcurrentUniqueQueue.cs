﻿namespace RokerPrime.Collections
{
    public class ConcurrentUniqueQueue<T> : UniqueQueue<T>
    {
        private readonly object _lock = new object();

        public override int Count {
            get {
                lock (_lock)
                {
                    return base.Count;
                }
            }
        }

        public override void Clear()
        {
            lock (_lock)
            {
                base.Clear();
            }
        }

        public override T Dequeue()
        {
            lock (_lock)
            {
                return base.Dequeue();
            }
        }

        public override void Enqueue(T t)
        {
            lock (_lock)
            {
                base.Enqueue(t);
            }
        }

        public override T Peek()
        {
            lock (_lock)
            {
                return base.Peek();
            }
        }
    }
}
