﻿using System.Collections;
using System.Collections.Generic;

namespace RokerPrime.Collections
{
    public class UniqueQueue<T> : IEnumerable<T>
    {
        private readonly HashSet<T> _hashSet = new HashSet<T>();
        private readonly Queue<T> _queue = new Queue<T>();

        public virtual int Count => _hashSet.Count;

        public virtual void Enqueue(T t)
        {
            if (_hashSet.Add(t))
            {
                _queue.Enqueue(t);
            }
        }

        public virtual void Clear()
        {
            _queue.Clear();
            _hashSet.Clear();
        }

        public virtual T Dequeue()
        {
            T t = _queue.Dequeue();
            _hashSet.Remove(t);
            return t;
        }

        public virtual T Peek()
        {
            return _queue.Peek();
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _queue.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
