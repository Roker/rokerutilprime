﻿using RokerPrime.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace RokerPrime.Collections
{
    public class ObservableListMirror<T>
    {
        private readonly INotifyCollectionChanged _observable;
        private readonly IList<T> _list;

        private readonly Func<object, T> _conversionFunc;

        public ObservableListMirror(INotifyCollectionChanged observableCollection, IList<T> list, Func<object, T> conversionFunc)
        {
            _observable = observableCollection;
            _observable.CollectionChanged += ObservableCollection_CollectionChanged;
            _conversionFunc = conversionFunc;
            _list = list;
        }

        private void ObservableCollection_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    InsertToList(e.NewStartingIndex, e.NewItems[0]);
                    return;
                case NotifyCollectionChangedAction.Remove:
                    _list.RemoveAt(e.OldStartingIndex);
                    return;
                case NotifyCollectionChangedAction.Move:
                    _list.Move(e.OldStartingIndex, e.NewStartingIndex);
                    return;
                case NotifyCollectionChangedAction.Reset:
                    _list.Clear();
                    return;
            }

        }

        private void InsertToList(int index, object o)
        {
            T t = _conversionFunc.Invoke(o);
            _list.Insert(index, t);
        }
    }
}
