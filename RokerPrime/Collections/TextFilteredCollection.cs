﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;

namespace RokerPrime.Collections
{
    public class TextFilteredCollection<T>
    {
        private readonly ObservableCollection<T> _filteredItems = new ObservableCollection<T>();
        private readonly PropertyInfo _filterProperty;

        private readonly string[] _filterProperties;

        private string _filterText;

        public string FilterText {
            get => _filterText;
            set {
                if (value != _filterText)
                {
                    _filterText = value;
                    ReFilter();
                }
            }
        }

        public ObservableCollection<T> SourceItems { get; } = new ObservableCollection<T>();

        private void SourceItems_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
            {
                foreach (T t in e.NewItems)
                {
                    Filter(t);
                }
            }
            else if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove)
            {
                foreach (T t in e.OldItems)
                {
                    _filteredItems.Remove(t);
                }
            }
            else if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Reset)
            {
                _filteredItems.Clear();
            }
            //ReFilter();
        }

        public ObservableCollection<T> Items => _filteredItems;


        public TextFilteredCollection(PropertyInfo filterProperty)
        {
            _filterProperty = filterProperty;
            SourceItems.CollectionChanged += SourceItems_CollectionChanged;
        }

        //public TextFilteredCollection(string[] filterProperties)
        //{
        //    _filterProperties = filterProperties;
        //    SourceItems.CollectionChanged += SourceItems_CollectionChanged;
        //}


        //public void Add(T t)
        //{
        //    _items.Add(t);
        //    Filter(t);
        //}
        //public void Remove(T t)
        //{
        //    _items.Remove(t);
        //    _filteredItems.Remove(t);
        //}

        //public void Clear()
        //{
        //    _items.Clear();
        //    _filteredItems.Clear();
        //}
        public void Add(List<T> ts)
        {
            foreach (T t in ts)
            {
                SourceItems.Add(t);
            }
        }

        private void ReFilter()
        {
            _filteredItems.Clear();
            Filter(SourceItems);
        }

        private void Filter(T t)
        {
            string propertyValue = (string)_filterProperty.GetValue(t);
            if (string.IsNullOrEmpty(_filterText) || propertyValue.ToLower().Contains(_filterText.ToLower()))
            {
                _filteredItems.Add(t);
            }
        }

        //private void Filter(T t)
        //{
        //    foreach(string fullPropertyName in _filterProperties)
        //    {
        //        GetProperty(t, fullPropertyName);

        //        //string propertyValue = 

        //    }
        //    //if (string.IsNullOrEmpty(_filterText) || propertyValue.ToLower().Contains(_filterText.ToLower()))
        //    //{
        //    //    _filteredItems.Add(t);
        //    //}
        //}

        //private bool GetProperty(object t, string fullPropertyName)
        //{
        //    Console.WriteLine("object: " + t);
        //    Console.WriteLine("FullPropertyName: " + fullPropertyName);
        //    string propertyName = fullPropertyName;
        //    if (fullPropertyName.Contains("."))
        //    {
        //        int sepIndex = fullPropertyName.IndexOf(".");
        //        propertyName = fullPropertyName.Substring(0, sepIndex);
        //        Console.WriteLine("propertyName: " + propertyName);

        //        PropertyInfo property = t.GetType().GetProperty(propertyName);
        //        object propertyValue = property.GetValue(t);

        //        return GetProperty(propertyValue, fullPropertyName.Substring(sepIndex + 1));
        //    }

        //    if (t is IEnumerable<object> enumerable)
        //    {
        //        foreach (object o in enumerable)
        //        {
        //            if (o.GetType().GetProperty(propertyName).GetValue(t) is string s)
        //            {
        //                return s.ToLower().Contains(_filterText.ToLower());
        //            }
        //        }
        //    }
        //    if (t.GetType().GetProperty(propertyName).GetValue(t) is string s)
        //    {
        //        return s.ToLower().Contains(_filterText.ToLower());
        //    }
        //    return false;
        //}


        private void Filter(ObservableCollection<T> ts)
        {
            foreach (T t in ts)
            {
                Filter(t);
            }
        }
    }
}
