﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace RokerPrime.Extensions
{
    //TODO: document/test
    public static class CollectionExtensions
    {
        public static void AddRange<T>(this ICollection<T> collection, IEnumerable<T> newObjects)
        {
            foreach (T t in newObjects)
            {
                collection.Add(t);
            }
        }

        public static List<T> GetRange<T>(this ObservableCollection<T> collection, int offset, int limit)
        {
            List<T> ts = new List<T>();
            for (int i = offset; i < offset + limit; i++)
            {
                ts.Add(collection[i]);
            }
            return ts;
        }
    }
}
