﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RokerPrime.Extensions
{
    //TODO: document/test
    public static class IEnumerableExtensions
    {

        public static async Task ForEachAsync<T>(this IEnumerable<T> enumerable, Func<T, Task> taskFunc)
        {
            foreach (T t in enumerable)
            {
                await taskFunc(t).ConfigureAwait(false);
            }
        }


        public static int IndexOf<T>(this IEnumerable<T> enumerable, T needle)
        {
            int index = 0;
            var comparer = EqualityComparer<T>.Default;
            foreach (T t in enumerable)
            {
                if (comparer.Equals(t, needle))
                {
                    return index;
                }
                index++;
            }
            return -1;
        }
    }
}
