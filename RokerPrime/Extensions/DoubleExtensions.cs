﻿namespace RokerPrime.Extensions
{
    //TODO: document/test
    public static class DoubleExtensions
    {
        public static string ToDollarString(this double d)
        {
            return "$" + d.ToString("N2");
        }
    }
}
