﻿using System;

namespace RokerPrime.Extensions
{
    //TODO: document/test
    public static class DateTimeExtensions
    {
        public static string ToFileSafeDateTime(this DateTime d)
        {
            return d.ToString("yyyy-dd-M--HH-mm-ss");
        }

        public static string ToMySQLDateTime(this DateTime d)
        {
            return d.ToString("yyyy-MM-dd H:mm:ss");
        }

        public static DateTime SetDay(this DateTime date, int day)
        {
            return date.AddDays(day - date.Day);
        }

        public static DateTime SetMonth(this DateTime date, int month)
        {
            return date.AddMonths(month - date.Month);
        }

        public static DateTime SetYear(this DateTime date, int year)
        {
            return date.AddYears(year - date.Year);
        }

        public static DateTime SetToLastOfMonth(this DateTime date)
        {
            return date.SetDay(DateTime.DaysInMonth(date.Year, date.Month));
        }

        public static DateTime NextDayOfWeek(this DateTime date, DayOfWeek dayOfWeek)
        {
            while (date.DayOfWeek != dayOfWeek)
            {
                date = date.AddDays(1);
            }
            return date.Date;
        }

        [Obsolete("Use MostRecentDayOfWeek() instead")]
        public static DateTime GetPreviousDayOfWeek(this DateTime date, DayOfWeek dayOfWeek)
        {
            while (date.DayOfWeek != dayOfWeek)
            {
                date = date.AddDays(-1);
            }
            return date.Date;
        }
        /*
         * Returns most recent date with day of week matching input.
         * 
         */
        public static DateTime MostRecentDayOfWeek(this DateTime date, DayOfWeek dayOfWeek)
        {
            DateTime previousDayOfWeek = new DateTime(date.Ticks);
            while (previousDayOfWeek.DayOfWeek != dayOfWeek)
            {
                previousDayOfWeek = previousDayOfWeek.AddDays(-1);
            }
            return previousDayOfWeek.Date;
        }

        public static long ToUnixTimestamp(this DateTime date)
        {
            return (long)date.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
        }
    }
}
