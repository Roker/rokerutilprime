﻿using System.Text.RegularExpressions;

namespace RokerPrime.Extensions
{
    //TODO: document/test
    public static class StringExtensions
    {
        public static string RemoveRepeatingSpaces(this string s)
        {
            return Regex.Replace(s, @"\s+", " ");
        }
        public static string TrimAndReduce(this string s)
        {
            return RemoveRepeatingSpaces(s).Trim();
        }
    }
}
