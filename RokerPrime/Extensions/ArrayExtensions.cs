﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace RokerPrime.Extensions
{
    public static class ArrayExtensions
    {

        //TODO: document
        public static List<TMember> GetPropertyList<TObj, TMember>(this IEnumerable<TObj> enumerable, Expression<Func<TObj, TMember>> expression)
        {
            List<TMember> tMembers = new List<TMember>();
            foreach (TObj obj in enumerable)
            {
                MemberExpression memberExpr = (MemberExpression)expression.Body;
                string memberName = memberExpr.Member.Name;
                Func<TObj, TMember> compiledDelegate = expression.Compile();
                tMembers.Add(compiledDelegate(obj));
            }

            return tMembers;
        }

        //TODO: document merge/delete
        public static TMember[] GetPropertyArray<TObj, TMember>(this TObj[] array, Expression<Func<TObj, TMember>> expression)
        {
            TMember[] tMembers = new TMember[array.Length];
            for (int i = 0; i < array.Length; i++)
            {
                MemberExpression memberExpr = (MemberExpression)expression.Body;
                string memberName = memberExpr.Member.Name;
                Func<TObj, TMember> compiledDelegate = expression.Compile();
                tMembers[i] = compiledDelegate(array[i]);
            }

            return tMembers;
        }
        //TODO: document merge/delete
        public static string[] GetStringPropertyArray<T>(this List<T> list, Func<T, string> getProp)
        {
            string[] values = new string[list.Count];
            for (int i = 0; i < list.Count; i++)
            {
                values[i] = getProp(list[i]);
            }
            return values;
        }
        //TODO: document merge/delete
        public static List<string> GetStringPropertyList<T>(this List<T> list, Func<T, string> getProp)
        {
            List<string> values = new List<string>();
            foreach (T t in list)
            {
                values.Add(getProp(t));
            }
            return values;
        }

        //TODO: test + document : should this be in matrix util or something
        public static T[][] To2D<T>(this T[] array)
        {
            T[][] newArray = new T[array.Length][];
            for (int i = 0; i < array.Length; i++)
            {
                newArray[i] = new T[] { array[i] };
            }
            return newArray;
        }


        //TODO: this is probably already in linq
        public static T[] Add<T>(this T[] array1, T[] array2)
        {
            if (array2 == null) return array1;
            T[] array3 = new T[array1.Length + array2.Length];
            Array.Copy(array1, array3, array1.Length);
            Array.Copy(array2, 0, array3, array1.Length, array2.Length);
            return array3;
        }
    }
}
