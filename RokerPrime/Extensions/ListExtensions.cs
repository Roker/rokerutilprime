﻿using System.Collections.Generic;

namespace RokerPrime.Extensions
{
    //TODO: document/test
    public static class ListExtensions
    {
        //https://stackoverflow.com/questions/450233/generic-list-moving-an-item-within-the-list
        //https://stackoverflow.com/users/6369/garry-shutler
        public static void Move<T>(this IList<T> list, int oldIndex, int newIndex)
        {
            T item = list[oldIndex];

            list.RemoveAt(oldIndex);

            list.Insert(newIndex, item);
        }
    }
}
