﻿using RokerPrime.Util;
using System.Collections.ObjectModel;

namespace RokerPrime.Matrix
{
    //TODO: document/test
    public class Matrix<T> : Collection<T> where T : class
    {
        private readonly int _columnCount;

        public Collection<T[]> GridCollection { get; protected set; }


        public Matrix(int columnCount) : this(columnCount, new Collection<T[]>())
        {
        }

        public Matrix(int columnCount, Collection<T[]> gridCollection)
        {
            _columnCount = columnCount;
            GridCollection = gridCollection;
        }

        protected override void ClearItems()
        {
            base.ClearItems();
            GridCollection.Clear();
        }
        protected override void InsertItem(int index, T item)
        {
            base.InsertItem(index, item);
            GridCollection.MatrixInsert(index, item, _columnCount);
        }

        protected override void RemoveItem(int index)
        {
            base.RemoveItem(index);
            GridCollection.MatrixRemove(index, _columnCount);
        }
        protected override void SetItem(int index, T item)
        {
            base.SetItem(index, item);
            GridCollection.MatrixReplace(index, item, _columnCount);
        }
    }
}
