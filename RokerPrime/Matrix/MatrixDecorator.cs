﻿using RokerPrime.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace RokerPrime.Matrix
{
    //TODO: document/test
    public class MatrixDecorator<T> where T : class
    {
        private int _columnCount;

        public Collection<T[]> GridCollection { get; protected set; }

        private INotifyCollectionChanged _notifyCollection;

        private bool _replaceRows;
        public bool ReplaceRows { get => _replaceRows; set => _replaceRows = value; }

        public int ColumnCount {
            get => _columnCount;
            set {
                _columnCount = value;
                ReInit();
            }
        }

        private void ReInit()
        {
            List<object> objects = new List<object>();
            foreach (object[] os in GridCollection)
            {
                foreach (object o in os)
                {
                    if (o != null)
                    {
                        objects.Add(o);
                    }
                }
            }
            GridCollection.Clear();
            foreach (object o in objects)
            {
                GridCollection.MatrixAdd(o as T, _columnCount, ReplaceRows);
            }
        }

        public INotifyCollectionChanged NotifyCollection {
            get => _notifyCollection;
            set {
                if (NotifyCollection != null)
                {
                    NotifyCollection.CollectionChanged -= NotifyCollection_CollectionChanged;
                }
                GridCollection.Clear();
                _notifyCollection = value;
                if (NotifyCollection != null)
                {
                    NotifyCollection.CollectionChanged += NotifyCollection_CollectionChanged;
                }
            }
        }


        public MatrixDecorator(int columnCount,
            Collection<T[]> gridCollection)
        {
            _columnCount = columnCount;
            GridCollection = gridCollection;
        }
        public MatrixDecorator(int columnCount,
            Collection<T[]> gridCollection,
            INotifyCollectionChanged notifyCollection) : this(columnCount, gridCollection)
        {
            NotifyCollection = notifyCollection;
        }

        public MatrixDecorator(int columnCount,
            Collection<T[]> gridCollection,
            IEnumerable<object> items) : this(columnCount, gridCollection)
        {
            foreach (object o in items)
            {
                GridCollection.MatrixAdd(o as T, _columnCount, ReplaceRows);
            }
        }

        private void NotifyCollection_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            try
            {
                if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
                {
                    GridCollection.MatrixInsert(e.NewStartingIndex, e.NewItems[0] as T, _columnCount, ReplaceRows);
                }
                else if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove)
                {
                    GridCollection.MatrixRemove(e.OldStartingIndex, _columnCount, ReplaceRows);
                }
                else if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Replace)
                {
                    GridCollection.MatrixReplace(e.NewStartingIndex, e.NewItems[0] as T, _columnCount, ReplaceRows);
                }
                else if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Reset)
                {
                    ClearGridCollection();
                }
                else if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Move)
                {
                    GridCollection.MatrixMove(e.OldStartingIndex, e.NewStartingIndex, _columnCount, ReplaceRows);
                }
            }
            catch
            {
                Console.WriteLine("Found");
                throw new Exception();
            }
        }

        //because of threadsafe ios issues
        //remove soon
        private void ClearGridCollection()
        {
            int count = GridCollection.Count;
            for (int i = 0; i < count; i++)
            {
                GridCollection.RemoveAt(0);
            }
        }


    }
}
